# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 16:51:32 2020

@author: ZeyuGao
"""

import torch
import torch.nn.functional as F
import numpy as np
import torch.nn as nn

class TruncatedLoss(nn.Module):

    def __init__(self, q=0.7, k=0.5, trainset_size=50000):
        super(TruncatedLoss, self).__init__()
        self.q = q
        self.k = k
        self.weight = torch.nn.Parameter(data=torch.ones(trainset_size, 1), requires_grad=False)
             
    def forward(self, logits, targets, indexes):
        p = F.softmax(logits, dim=1)
        Yg = torch.gather(p, 1, torch.unsqueeze(targets, 1))

        loss = ((1-(Yg**self.q))/self.q)*self.weight[indexes] + ((1-(self.k**self.q))/self.q)*(1-self.weight[indexes])
        loss = torch.mean(loss)

        return loss

    def update_weight(self, logits, targets, indexes):
        p = F.softmax(logits, dim=1)
        Yg = torch.gather(p, 1, torch.unsqueeze(targets, 1))
        Lq = ((1-(Yg**self.q))/self.q)
        Lqk = np.repeat(((1-(self.k**self.q))/self.q), targets.size(0))
        Lqk = torch.from_numpy(Lqk).type(torch.cuda.FloatTensor)
        Lqk = torch.unsqueeze(Lqk, 1)

        condition = torch.gt(Lqk, Lq)
        self.weight[indexes] = condition.type(torch.cuda.FloatTensor)

class WeaklySuperLoss(nn.Module):
    
    def __init__(self):
        super(WeaklySuperLoss, self).__init__()

    def forward(self, outputs, subtypes):
        #####subtype loss#######
        outputs_soft = torch.nn.functional.softmax(outputs, 1)
        st_loss = outputs_soft.clone()[:,1:]
        subtypes_onehot = torch.zeros_like(st_loss)
        subtypes_onehot.zero_()
        subtypes_onehot.scatter_(1, subtypes.unsqueeze(-1), 1)
        for i in range(len(outputs)):
            st_loss[i][int(subtypes[i])] += outputs_soft[i][0]
        st_loss = -subtypes_onehot.float() * torch.log(st_loss+1e-15)
        st_loss = torch.mean(st_loss)
        return st_loss


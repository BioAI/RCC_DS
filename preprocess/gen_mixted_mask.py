# -*- coding: utf-8 -*-
import os
from tqdm import tqdm 
import cv2 as cv

def gen_mixed_mask(binary_mask_folder, cancer_mask_folder, mixed_mask_folder):
    cancer_mask_files = os.listdir(cancer_mask_folder)
    i=0
    for cancer_mask_file_name in tqdm(cancer_mask_files):
        cancer_mask_path = cancer_mask_folder + '/' + cancer_mask_file_name
        binary_mask_path = binary_mask_folder + '/' + cancer_mask_file_name
        if(not os.path.exists(binary_mask_path)):
            print(i)
            i = i+1
            continue
        cancer_mask = cv.imread(cancer_mask_path, 0)
        binary_mask = cv.imread(binary_mask_path, 0)
        if(cancer_mask.shape == binary_mask.shape and not (cancer_mask.any() == 0)):
            mixed_mask = cancer_mask + binary_mask
            mixed_mask_path = mixed_mask_folder + '/' + cancer_mask_file_name
            print(mixed_mask_path)
            cv.imwrite(mixed_mask_path, mixed_mask)



# -*- coding: utf-8 -*-
import cv2
import openslide
import numpy as np
from PIL import Image
from tqdm import tqdm
from utils import get_all_files

def generate_binary_mask_for_wsi(file_path, output_folder):
    oslide = openslide.OpenSlide(file_path)
    level = oslide.level_count - 1
    w, h = oslide.level_dimensions[level]
    if level < 1:
        print(file_path)
        oslide.close()
        return
        patch = oslide.read_region((0, 0), 0, (w, h))
        patch = patch.resize((int(w/32), int(h/32)), Image.ANTIALIAS)
    else:
        patch = oslide.read_region((0, 0), level, (w, h))
    slide_id = file_path.split('/')[-1].split('.svs')[0]
    patch.save('{}/{}_resized.png'.format(output_folder, slide_id));
    img = cv2.cvtColor(np.asarray(patch),cv2.COLOR_RGB2GRAY)
    img = cv2.GaussianBlur(img, (61, 61), 0)
    ret, img_filtered = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    fname = '{}/{}_mask.png'.format(output_folder, slide_id)
    cv2.imwrite(fname, img_filtered)
    oslide.close()
    
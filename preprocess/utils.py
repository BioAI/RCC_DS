# -*- coding: utf-8 -*-
import os
import numpy as np
import cv2
import random
from tqdm import tqdm


def get_all_files(path):
    file_list = []
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in filenames:
            file_list.append(os.path.join(dirpath,filename).replace('\\', '/'))
    return file_list


def get_mean_std(imgs_path):
    class CalMeanVar():
        def __init__(self):
            self.count = 0
            self.A = 0
            self.A_ = 0
            self.V = 0
    
        def cal(self, data):
            self.count += 1
            if self.count == 1:
                self.A_ = data
                self.A = data
                return
            self.A_ = self.A
            self.A = self.A + (data - self.A) / self.count
            self.V = (self.count - 1) / self.count ** 2 * (data - self.A_)**2 + (self.count - 1)/self.count * self.V
    
    img_h, img_w = 64, 64   
    num_C = 10000
    imgs_path_list = get_all_files(imgs_path)
    random.shuffle(imgs_path_list)
    B=CalMeanVar()
    G=CalMeanVar()
    R=CalMeanVar()
    for item in tqdm(imgs_path_list[:num_C]):
        img = cv2.imread(item)
        img = cv2.resize(img,(img_w,img_h))
        img = img/255
        b_list = img[:,:,0].ravel()
        g_list = img[:,:,1].ravel()
        r_list = img[:,:,2].ravel()
        for i in range(len(b_list)):
            B.cal(b_list[i])
            G.cal(g_list[i])
            R.cal(r_list[i])
    print("normMean = {}".format([R.A, G.A, B.A]))
    print("normStd = {}".format([np.sqrt(R.V),np.sqrt(G.V),np.sqrt(B.V)]))

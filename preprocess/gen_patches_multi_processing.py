# -*- coding: utf-8 -*-
import os
import cv2
import openslide
from tqdm import tqdm
from PIL import Image
import time, threading
from utils import get_all_files
    
def cut_patches_from_wsi(flag, file_path, output_folder, binary_mask_folder, cancer_mask_folder,
                         size=1000, step=500, binary_rate=0.5, cancer_rate=0.1, output_size=512):
    slide_id = file_path.split('/')[-1].split('.svs')[0]
    birnary_mask_path = '{}/{}_mask.png'.format(binary_mask_folder, slide_id)
    cancer_mask_path = '{}/{}_mask.png'.format(cancer_mask_folder, slide_id)
    if not (os.path.exists(birnary_mask_path) and os.path.exists(cancer_mask_path)):
        print("No mask file for this WSI!")
        return
    output_folder = os.path.join(output_folder, slide_id)
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    oslide = openslide.OpenSlide(file_path)
    width = oslide.dimensions[0]
    height = oslide.dimensions[1]
    w, h = oslide.level_dimensions[-1]
    mag_w = width/w
    mag_h = height/h
    mag_size = size/mag_w
    binary_mask = cv2.imread(birnary_mask_path, 0)
    cancer_mask = cv2.imread(cancer_mask_path, 0)
    binary_mask = binary_mask.T 
    cancer_mask = cancer_mask.T 
    if not (binary_mask.shape == (w, h) and cancer_mask.shape == (w, h)):
        print("Mask file not match for this WSI!")
        return
    corrs = []
    for x in range(1, width, step):
        for y in range(1, height, step):
            if x + size > width:
                continue
            else:
                w_x = size
            if y + size > height:
                continue
            else:
                w_y = size
            binary_mask_patch = binary_mask[int(x/mag_w):int(x/mag_w + mag_size),int(y/mag_h):int(y/mag_h + mag_size)]
            cancer_mask_patch = cancer_mask[int(x/mag_w):int(x/mag_w + mag_size),int(y/mag_h):int(y/mag_h + mag_size)]
            binary_mask_number = binary_mask_patch[(binary_mask_patch == 255)].size  
            cancer_mask_number = cancer_mask_patch[(cancer_mask_patch == 255)].size
            if(flag == 'normal'):
                if((binary_mask_number < binary_mask_patch.size * binary_rate) and (cancer_mask_number == 0)):
                    corrs.append((x, y, w_x, w_y))
            elif(flag == 'cancer'):
                if((binary_mask_number < binary_mask_patch.size * binary_rate) and (cancer_mask_number >= cancer_mask_patch.size * cancer_rate)):
                    corrs.append((x, y, w_x, w_y))
    for corr in tqdm(corrs):
        x, y, w_x, h_y = corr
        patch = oslide.read_region((x, y), 0, (w_x, h_y))
        patch = patch.resize((output_size, output_size), Image.ANTIALIAS)
        fname = '{}/{}_{}_{}.png'.format(output_folder, x, y, size)
        patch.save(fname)
    oslide.close()
  

# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 10:52:16 2019

@author: ZeyuGao
"""

import argparse
import os
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
from tqdm import tqdm
import numpy as np
from torchvision import models
from load_patches_data import PatchesDataset
from PIL import ImageFile
import pandas as pd

def label_generate(test_files, result_files, output_files):
    def sigmoid(x):
        return 1 / (1 + math.exp(-x))
    test_set = pd.read_csv(test_files, header=None, sep=' ')
    test_set.columns = ['filename','label','subtype']
    predict_all = pd.read_csv(result_files, header=None)
    predict_all.columns = ['cancer','normal']
    predict_all['normal'] = predict_all['normal'].apply(lambda x: sigmoid(x))
    predict_all['cancer'] = predict_all['normal'].apply(lambda x: 1-x)
    test_set = pd.concat([test_set, predict_all], axis=1)
    test_set['predict'] = test_set['cancer'].apply(lambda x: 0 if x>0.5 else 1)
    test_set['predict'] = test_set['predict'] * (test_set['subtype']+1)
    test_set[['filename', 'predict']].to_csv(output_files, header=None, index=False, sep=' ')

ImageFile.LOAD_TRUNCATED_IMAGES = True
parser = argparse.ArgumentParser(description='RCC prediction')
parser.add_argument('--epochs', default=1024, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--test_files', type=str)
parser.add_argument('--output_files', default='./outputs_temp.csv', type=str)
parser.add_argument('--file_path_base', type=str, help='model file path')
parser.add_argument('--num_classes', default=2, type=int)
parser.add_argument('--gpu', default='0', type=str,
                    help='id(s) for CUDA_VISIBLE_DEVICES')


args = parser.parse_args()
os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

result_files = "./result_temp.csv"

normMean = [0.744, 0.544, 0.670]
normStd = [0.183, 0.245, 0.190]
normTransform = transforms.Normalize(normMean, normStd)
test_transform = transforms.Compose([
        transforms.Resize(224),
        transforms.ToTensor(),
        normTransform
    ])
test_data = PatchesDataset(args.test_files, transform=test_transform)
test_loader = torch.utils.data.DataLoader(
        test_data, batch_size=128, shuffle=False,
        num_workers=4, pin_memory=False)

base_model = models.resnet34()
base_model.fc = nn.Linear(base_model.fc.in_features, args.num_classes)
base_model = base_model.cuda()
base_model = torch.nn.DataParallel(base_model)

checkpoint = torch.load(args.file_path_base)
base_model.load_state_dict(checkpoint['net'])
cudnn.benchmark = True
criterion = nn.CrossEntropyLoss().cuda()
base_model.eval()
with torch.no_grad():
    for batch_idx, (inputs, targets) in enumerate(tqdm(test_loader)):
        f = open(result_files,'ab')
        inputs, targets = inputs.to('cuda'), targets.type(torch.LongTensor).to('cuda')
        outputs = base_model(inputs)
        np.savetxt(f, outputs.cpu().data.numpy(), delimiter=",", fmt='%.4f')
        f.close()
if num_classes == 2:
    label_generate(args.test_files, result_files, args.output_files)

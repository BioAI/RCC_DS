# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 16:11:00 2019

@author: ZeyuGao

"""

import argparse
import os
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import matplotlib
import matplotlib.pyplot as plt
from tqdm import tqdm
from sklearn.metrics import classification_report
from torchvision import models

from load_patches_data import PatchesDataset, PatchesDatasetSubtype
from Losses import TruncatedLoss, WeaklySuperLoss

matplotlib.use('Agg')

parser = argparse.ArgumentParser(description='subetype cancer detection')
parser.add_argument('--epochs', default=60, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--model_path_base', default='./subtype_model/checkpoint.pth', type=str)
parser.add_argument('--model_path_best', default='./subtype_model/model_best.pth', type=str)
parser.add_argument('--gpu', default='0', type=str,
                    help='id(s) for CUDA_VISIBLE_DEVICES')
parser.add_argument('--train_files', type=str)
parser.add_argument('--test_files', type=str)
parser.add_argument('--augment', default=True type=bool)
parser.add_argument('--batch_size', default=256, type=int)
parser.add_argument('--start_prune', default=2, type=int)
parser.add_argument('--num_workers', default=4, type=int)
parser.add_argument('--seed', default=1, type=int)
parser.add_argument('--lr', default=0.0001, type=float)
parser.add_argument('--num_classes', default=4, type=int)
parser.add_argument('--start_epoch', default=0, type=int)
parser.add_argument('--lambda_u', default=5, type=int)

args = parser.parse_args()
os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu


def build_dataset(train_files, test_files):
    normMean = [0.744, 0.544, 0.670]
    normStd = [0.183, 0.245, 0.190]
    normTransform = transforms.Normalize(normMean, normStd)
    if args.augment:
        train_transform = transforms.Compose([
            transforms.Resize(224),
            transforms.RandomCrop(224, padding=64, padding_mode='reflect'),
            transforms.RandomHorizontalFlip(),
            transforms.RandomRotation((-90,90)), 
            transforms.ToTensor(),
            normTransform,
        ])
    else:
        train_transform = transforms.Compose([
            transforms.Resize(224),
            transforms.ToTensor(),
            normTransform,
        ])
    test_transform = transforms.Compose([
        transforms.Resize(224),
        transforms.ToTensor(),
        normTransform
    ])
    train_data = PatchesDatasetSubtype(train_files, transform=train_transform)
    train_loader = torch.utils.data.DataLoader(
            train_data, batch_size=args.batch_size, shuffle=True,
            num_workers=args.num_workers, pin_memory=True)
    test_data = PatchesDataset(test_files, transform=test_transform )
    test_loader = torch.utils.data.DataLoader(
            test_data, batch_size=args.batch_size, shuffle=False,
            num_workers=args.num_workers, pin_memory=True)
    return train_loader, test_loader

def plot_metrics(save_path):
    fig, axes = plt.subplots(1, 2, figsize=(13, 5))
    ax1, ax2 = axes.ravel()
    ax1.plot(model_train_loss, label='model_train_loss')
    ax1.plot(model_test_loss, label='model_test_loss')
    ax1.set_ylabel("Losses")
    ax1.set_xlabel("Epochs")
    ax1.legend()
    ax2.plot(train_acc, label='train_accuracy')
    ax2.plot(test_acc, label='test_accuracy')
    ax2.set_ylabel('Accuracy')
    ax2.set_xlabel('Epochs')
    ax2.legend()
    plt.savefig(save_path)
    
def make_check_point(file_path_base):
    state_base = {
    'epoch': epoch,
    'net': base_model.state_dict(),
    'optimizer': optimizer_base.state_dict(),
    }
    torch.save(state_base, file_path_base)
    print("base model in epoch %s save to %s" % (epoch, file_path_base))
    
def load_check_point(file_path_base):
    checkpoint = torch.load(file_path_base)
    base_model.load_state_dict(checkpoint['net'])
    optimizer_base.load_state_dict(checkpoint['optimizer'])
    epoch = checkpoint['epoch'] + 1
    return epoch
    
def train(epoch):
    train_loss = 0
    correct = 0
    total = 0
    print('\nepoch: %d' % epoch)
    
    # if (epoch+1) >= args.start_prune and (epoch+1) % 5 == 0:
    #     checkpoint = torch.load(file_path_best)
    #     net = models.resnet34()
    #     net.fc = nn.Linear(net.fc.in_features, args.num_classes)
    #     net = torch.nn.DataParallel(net)
    #     net = net.cuda()
    #     net.load_state_dict(checkpoint['net'])
    #     net.eval()
    #     for batch_idx, (inputs, targets, subtypes, indexes) in enumerate(train_loader):
    #         inputs, targets = inputs.cuda(), targets.cuda()
    #         outputs = net(inputs)
    #         criterion_1.update_weight(outputs, targets, indexes)
    
    base_model.train()
    for batch_idx, (inputs, targets, subtypes, indexes) in enumerate(tqdm(train_loader)):
        inputs, targets, subtypes = inputs.to('cuda'), targets.type(torch.LongTensor).to('cuda'), subtypes.type(torch.LongTensor).to('cuda')
        #####################################################
        ###update base model by meta model & training data###
        #####################################################
        outputs = base_model(inputs)
        loss = criterion(outputs, targets) + args.lambda_u*criterion_2(outputs, subtypes)
        # updata base model params
        optimizer_base.zero_grad()
        loss.backward()
        optimizer_base.step()
        # report sth.
        train_loss += loss.item()
        _, predicted = outputs.max(1)
        total += targets.size(0)
        correct += predicted.eq(targets).sum().item()
        # print(loss)
        
    print('Train Loss: %.3f | Train Acc: %.3f%% (%d/%d)'
        % (train_loss/(batch_idx+1), 100.*correct/total, correct, total))
    train_acc.append(100.*correct/total)
    model_train_loss.append(train_loss/(batch_idx+1))
    scheduler.step(train_loss/(batch_idx+1))

def test(epoch):
    base_model.eval()
    test_loss = 0
    correct = 0
    total = 0
    predicted_all = []
    outputs_all = []
    target_names = ['normal', 'ccRcc', 'pRcc', 'chRcc']
    with torch.no_grad():
        for batch_idx, (inputs, targets) in enumerate(tqdm(test_loader)):
#            inputs = to_var(inputs, requires_grad=False)
#            targets = to_var(targets, requires_grad=False)
            inputs, targets = inputs.to('cuda'), targets.type(torch.LongTensor).to('cuda')
            outputs = base_model(inputs)
            loss = criterion(outputs, targets)
            test_loss += loss.item()
            _, predicted = outputs.max(1)
            total += targets.size(0)
            correct += predicted.eq(targets).sum().item()
            predicted_all += predicted.cpu().data.numpy().tolist()
            outputs_all += targets.cpu().data.numpy().tolist()
        print('Test Loss: %.3f | Test Acc: %.3f%% (%d/%d)'
              % (test_loss/(batch_idx+1), 100.*correct/total, correct, total))
        print(classification_report(outputs_all, predicted_all, target_names=target_names))
        model_test_loss.append(test_loss/(batch_idx+1))
        test_acc.append(100.*correct/total)

def main():
    train_files = args.train_files
    test_files = args.test_files
    train_loader, test_loader = build_dataset(train_files, test_files)
    
    # build model
    base_model = models.resnet34(pretrained=True)
    base_model.fc = nn.Linear(base_model.fc.in_features, args.num_classes)
    base_model = base_model.cuda()
    base_model = torch.nn.DataParallel(base_model)
    optimizer_base = optimizer = torch.optim.Adam(base_model.parameters(), lr=args.lr)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer_base, 'min',factor=0.1, patience=5, verbose=True)
    cudnn.benchmark = True
    # weights
    weight = torch.Tensor([1, 1, 2, 3])
    criterion = nn.CrossEntropyLoss(weight=weight).cuda()
    #criterion_1 = TruncatedLoss(trainset_size=len(train_loader.dataset)).cuda()
    criterion_2 = WeaklySuperLoss().cuda()
    
    if args.start_epoch>0:
        epoch = args.start_epoch
        start_epoch = load_check_point(model_path_base)
    else:
        start_epoch = 0
    
    # record sth.
    train_acc = []
    test_acc = []
    model_train_loss = []
    model_test_loss = []
    best_acc = 0
    # training
    for epoch in range(start_epoch, args.epochs):
        train(epoch)
        test(epoch)
        if test_acc[-1] > best_acc:
            best_acc = test_acc[-1]
            make_check_point(model_path_best)
        make_check_point(model_path_base)
    plot_metrics('./metrics.jpg')

if __name__ == '__main__':
    main()

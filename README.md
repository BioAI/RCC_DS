# RCC Detection and Subtyping Framework

Code for paper "Renal Cell Carcinoma Detection and Subtyping with Minimal Point-Based Annotation in Whole-Slide Images".  

The original dataset comes from [TCGA](https://portal.gdc.cancer.gov/repository)  (KIRC, KIRP, KICH) project. 

The annotated dataset is released on https://dataset.chenli.group/home/rcc-region-and-subtyping.

## Setups
The requirement is as bellow:  

* Python 3+
* PyTorch 1.3.1
* Torchvision 0.4.2
* numpy 1.16.2
* PIL 6.2.1
* matplotlib 3.0.2
* tqdm 4.28.1
* sklearn 0.20.1
* Openslide 1.1.1
* cv2 4.1.2.30
* pandas 0.23.4

## Running
### Detection (CCRCC example)
```bash
# train
python RCC_detection_train.py --epochs 200 --labeled_data_files your_labeled_images.txt --unlabeled_files your_unlabeled_images.txt --test_files valid_images.txt --out rcc@detection
# finetune
python RCC_detection_train.py --epochs 205 --resume rcc@detection/checkpoint.pth.tar --transfer True --labeled_data_files your_labeled_images.txt --unlabeled_files other_unlabeled_images.txt --test_files valid_images.txt --out rcc@detection@transfer
# predict
python RCC_predict.py --num_classes 2 --file_path_base rcc@detection@transfer/checkpoint.pth.tar --test_files your_unlabeled_images.txt --output_files your_unlabeled_predicted.txt
python RCC_predict.py --num_classes 2 --file_path_base rcc@detection@transfer/checkpoint.pth.tar --test_files other_unlabeled_images.txt --output_files other_unlabeled_predicted.txt
cat your_unlabeled_predicted.txt,other_unlabeled_predicted.txt -> ccrcc_predicted.txt
```
### Subtyping
```bash
# train
cat ccrcc_predicted.txt,prcc_predicted.txt,chrcc_predicted.txt -> subtype_train_images.txt
python RCC_subtyping_train.py --num_classes 4 --model_path_base ./subtype_model/checkpoint.pth --model_path_best ./subtype_model/model_best.pth --train_files subtype_train_images.txt --test_files subtype_valid_images.txt 
# predict
python RCC_predict.py --num_classes 4 --file_path_base ./subtype_model/model_best.pth --test_files subtype_test_images.txt
```
